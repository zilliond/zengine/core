<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            if (setting('balances_type') === 'shared') {
                $table->unsignedBigInteger('wallet_id')->nullable();
            } elseif (setting('balances_type') === 'divided') {
                $table->unsignedBigInteger('wallet_id');
            }

            $table->decimal('amount', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'));
            $table->unsignedInteger('currency_id');
            $table->unsignedBigInteger('payment_system_id')->nullable();
            $table->json('payment_system_data')->nullable();
            $table->string('type');
            $table->string('status')->default(app('zengine')->modelClass('Operation')::STATUS_CREATED);
            $table->string('comment')->nullable();

            $table->timestamp('status_changed_at')->nullable();
            $table->timestamps();

            $table->nullableMorphs('target');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('payment_system_id')->references('id')->on('payment_systems');
            $table->foreign('wallet_id')->references('id')->on('wallets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
