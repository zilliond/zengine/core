<?php

use Modules\Core\Models\CurrencyRate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('currency_id');
            $table->unsignedInteger('target_currency_id');
            $table->decimal('rate', setting('currencies.rate.decimal_total'), setting('currencies.rate.decimal_precision'));
            $table->string('type')->default(app('zengine')->modelClass('CurrencyRate')::TYPE_API);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_rates');
    }
}
