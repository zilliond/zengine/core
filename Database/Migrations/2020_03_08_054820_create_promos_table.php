<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('name');
            $table->decimal('cost')->default(0);
            $table->decimal('deposit')->default(0);
            $table->boolean('deposit_included')->default(false);
            $table->timestamp('posted_at');
            $table->timestamp('ended_at')->nullable();
            $table->string('link')->nullable();
            $table->string('user_nick')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
