<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $settings = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = config('settings.defaults');
        $this->getRecursiveKeys($settings);
        setting($this->settings)->save();
        setting(['general.start_date' => now()->toDateString()])->save();
    }

    /**
     * @param $settings
     * @param  string  $prefix
     */
    protected function getRecursiveKeys($settings, $prefix = '')
    {
        foreach ($settings as $key => $value) {
            if (is_array($value)) {
                $this->getRecursiveKeys($value, $prefix.$key.'.');
            } else {
                $this->settings[$prefix.$key] = $value;
            }
        }
    }
}
