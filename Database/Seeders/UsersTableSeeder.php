<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(app('zengine')->modelClass('User'), 50)->make();
        $users->each(static function (User $user, $index) {
            $user->email = "user$index@site.com";
            $user->login = "user$index";
            if ($inviter = app('zengine')->model('User')->inRandomOrder()->first()) {
                $user->inviter_id = $inviter->id;
            }
            $user->save();
            $userService = app('users');
            $userService->onCreate($user);
        });

        $admin = app('zengine')->model('User')->find(1);
        $faker = \Faker\Factory::create();
        app('zengine')->model('User')->chunk(50, static function ($users) use ($admin, $faker) {
            foreach ($users as $user) {
                $ticket = $user->tickets()->create([
                    'subject' => 'Старт',
                ]);
                $ticket->messages()->create([
                    'user_id' => $admin->id,
                    'message' => 'Тестирование проекта',
                ]);
                $max = random_int(3, 6);
                for ($i = 0; $i < $max; $i++) {
                    $ticket->messages()->create([
                        'user_id' => $faker->randomElement([$user->id, $admin->id]),
                        'message' => $faker->text,
                    ]);
                }
                if (setting('balances_type') === 'divided') {
                    $payment_systems = app('zengine')->model('PaymentSystem')->inRandomOrder()->limit($faker->numberBetween(
                        1,
                        3
                    ))->get();
                    foreach ($payment_systems as $payment_system) {
                        /** @var $payment_system \Modules\Core\Models\PaymentSystem */
                        $user->wallets()->create([
                            'payment_system_id' => $payment_system->id,
                            'balance'           => $faker->numberBetween(100, 300),
                            'wallet'            => $faker->creditCardNumber
                        ]);
                    }
                }
            }
            unset($users);
        });
        $this->command->info($users->count() . ' Users created.');
        \Artisan::call('passport:install');
    }
}
