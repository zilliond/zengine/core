<?php

return [
    'balance' => [
        'not_enough'    => 'User does not have enough funds',
        'wallet_belong' => 'The wallet does not belong to the user',
    ],
];
