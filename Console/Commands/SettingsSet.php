<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;

class SettingsSet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zengine:settings:set {key : Key of settings} {value : Value of setting}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set setting value by key';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = $this->argument('key');
        $value = $this->argument('value');
        $setting = setting($key);
        if (null === $setting) {
            $this->warn('Key not exist, creating new setting');
        }
        setting([$key => $value])->save();
        $setting = setting($key);
        $this->info("New value of {$key}: {$setting}");

        return true;
    }
}
