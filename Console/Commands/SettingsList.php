<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;

class SettingsList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zengine:settings:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all settings with values';

    protected static $data = [];

    /**
     * Execute the console command.
     */
    public function handle(): bool
    {
        $headers = ['Key', 'Value', 'Label'];
        $settings = setting()->all();
        foreach ($settings as $key => $value) {
            if (is_array($value)) {
                $this->recursionPrepare($key, $value);
            } else {
                self::$data[] = [
                    'key'   => $key,
                    'value' => $value,
                    'label' => config('settings.fields.'.$key.'.label'),
                ];
            }
        }
        $this->info('Settings table');
        $this->table($headers, self::$data);

        return true;
    }

    public function recursionPrepare($prefix, $data): void
    {
        foreach ($data as $innerKey => $value) {
            $key = $prefix.'.'.$innerKey;
            if (is_array($value)) {
                $this->recursionPrepare($key, $value);
            } else {
                self::$data[] = [
                    'key'   => $key,
                    'value' => $value,
                    'label' => config('settings.fields.'.$key.'.label'),
                ];
            }
        }
    }
}
