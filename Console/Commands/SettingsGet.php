<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;

class SettingsGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zengine:settings:get
                            {key : The key of setting}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a setting value for key';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = $this->argument('key');
        $setting = setting($key);
        if (null === $setting) {
            $this->error('Setting with this key not find');

            return false;
        }
        $this->info("Value of {$key}: {$setting}");

        return true;
    }
}
