<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Services\DepositService;

class DepositProcessAccrual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zengine:deposits:process-accrual {id? : Deposit id for accrual}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deposit process accrual';

    /**
     * @var \Modules\Core\Services\DepositService
     */
    protected $depositService;

    /**
     * Create a new command instance.
     * @param  DepositService  $depositService
     */
    public function __construct(DepositService $depositService)
    {
        parent::__construct();
        $this->depositService = $depositService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function handle()
    {
        if ((bool) setting('general.accrual_enabled', true)) {
            $deposits = app('zengine')->model('Deposit')->with(['plan', 'currency'])->where('next_accrual_at', '<=', now())->where('left_accruals', '>', 0)->get();
            \Log::info("zengine:deposits:process-accrual: Processing {$deposits->count()} deposits [{$deposits->pluck('id')->implode(',')}]");
            foreach ($deposits as $deposit) {
                $this->depositService->processAccrual($deposit);
            }
        } else {
            \Log::info('zengine:deposits:process-accrual: Processing deposits disabled');
        }
    }
}
