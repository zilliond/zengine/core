<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Models\Operation;
use Modules\Core\Payments\PaymentSystemProvider;
use Modules\Core\Services\OperationService;

class PaymentSystemOperationStatusUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:operation:status-update {id? : Operation id for check}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check operation status of payment system';

    /**
     * @var \Modules\Core\Services\OperationService
     */
    protected $operationService;

    /**
     * Create a new command instance.
     * @param  \Modules\Core\Services\OperationService  $operationService
     */
    public function __construct(OperationService $operationService)
    {
        parent::__construct();
        $this->operationService = $operationService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function handle()
    {
        $operation_id = $this->argument('id');
        if ($operation_id) {
            $operation = app('zengine')->model('Operation')->find($operation_id);
            $this->processOperation($operation);
        } else {
            $operations = app('zengine')->model('Operation')->whereIn('type', [
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW,
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL,
            ])->whereIn('status', [
                    app('zengine')->modelClass('Operation')::STATUS_CREATED,
                    app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS,
                ])->whereNotNull('payment_system_id')->get();
            \Log::channel('console')->info("PaymentSystemOperationStatusUpdate|Find {$operations->count()} operations for check_status");
            foreach ($operations as $operation) {
                if (!$this->processOperation($operation)) {
                    \Log::channel('console')->info("PaymentSystemOperationStatusUpdate|Operation {$operation->id} check status false");
                }
            }
        }
    }

    public function processOperation(Operation $operation)
    {
        if (!$operation->payment_system_id) {
            return false;
        }
        if (!in_array($operation->type, [
            app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW,
            app('zengine')->modelClass('Operation')::TYPE_USER_REFILL,
        ], true)) {
            return false;
        }
        if (!in_array($operation->status, [
            app('zengine')->modelClass('Operation')::STATUS_CREATED,
            app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS,
        ], true)) {
            return false;
        }
        $ps = $operation->payment_system;
        /** @var PaymentSystemProvider $paymentProvider */
        $paymentProvider = $ps->getProvider();
        if ($ps->credentials && is_array($ps->credentials)) {
            $paymentProvider->setCredentials($ps->credentials);
        }
        if (method_exists($paymentProvider, 'checkStatus')) {
            return $paymentProvider->checkStatus($operation);
        }
        $this->info('provider check status method not exist');
        \Log::channel('console')->info("PaymentSystemOperationStatusUpdate|Operation {$operation->id}|provider check status method not exist");

        return false;
    }
}
