<?php

namespace Modules\Core\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Modules\Core\Console\Commands\DepositProcessAccrual;
use Modules\Core\Console\Commands\GenerateJavascriptTranslation;
use Modules\Core\Console\Commands\Migrate;
use Modules\Core\Console\Commands\PaymentSystemOperationStatusUpdate;
use Modules\Core\Console\Commands\RecalculateOnlineUsers;
use Modules\Core\Console\Commands\SettingsGet;
use Modules\Core\Console\Commands\SettingsList;
use Modules\Core\Console\Commands\SettingsSet;
use Modules\Core\Console\Commands\UpdateCurrenciesRates;

class ZEngineKernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Migrate::class,
        DepositProcessAccrual::class,
        RecalculateOnlineUsers::class,
        GenerateJavascriptTranslation::class,
        PaymentSystemOperationStatusUpdate::class,
        UpdateCurrenciesRates::class,

        SettingsList::class,
        SettingsGet::class,
        SettingsSet::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('zengine:deposits:process-accrual')->everyMinute();
        $schedule->command('zengine:users:online')->everyMinute();
        $schedule->command('app:operation:status-update')->everyTenMinutes(); //TODO: refactor command name
        if ('local' === config('app.env')) {
            $schedule->command('zengine:js:generate-translations --admin')->everyMinute();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        if (\File::isDirectory(app_path('Console/Commands'))) {
            $this->load(app_path('Console/Commands'));
        }
    }
}
