<?php


namespace Modules\Core\Social;

use SocialiteProviders\Manager\SocialiteWasCalled;

class GoogleExtendSocialite
{
    /**
     * Execute the provider.
     * @param  SocialiteWasCalled  $socialiteWasCalled
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('google', GoogleProvider::class);
    }
}
