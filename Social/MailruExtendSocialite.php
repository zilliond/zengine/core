<?php


namespace Modules\Core\Social;

use SocialiteProviders\Manager\SocialiteWasCalled;

class MailruExtendSocialite
{
    /**
     * Execute the provider.
     * @param  SocialiteWasCalled  $socialiteWasCalled
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('mailru', MailruProvider::class);
    }
}
