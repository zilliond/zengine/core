<?php

namespace Modules\Core\Social;

use Arr;
use SocialiteProviders\Graph\Provider;
use SocialiteProviders\Manager\OAuth2\User;

class MicrosoftProvider extends Provider
{

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        // Mapping default Laravel user keys to the keys that are nested in the
        // response from the provider.
        return (new User())->setRaw($user)->map([
            'id'    => $user['id'],
            'name'  => $user['displayName'],
            'email' => $user['userPrincipalName'],

            // The following values are not always required by the provider. We
            // cannot guarantee they will exist in the $user array.
            'businessPhones'    => Arr::get($user, 'businessPhones'),
            'displayName'       => Arr::get($user, 'displayName'),
            'givenName'         => Arr::get($user, 'givenName'),
            'jobTitle'          => Arr::get($user, 'jobTitle'),
            'mail'              => Arr::get($user, 'mail'),
            'mobilePhone'       => Arr::get($user, 'mobilePhone'),
            'officeLocation'    => Arr::get($user, 'officeLocation'),
            'preferredLanguage' => Arr::get($user, 'preferredLanguage'),
            'surname'           => Arr::get($user, 'surname'),
            'userPrincipalName' => Arr::get($user, 'userPrincipalName'),
        ]);
    }
}
