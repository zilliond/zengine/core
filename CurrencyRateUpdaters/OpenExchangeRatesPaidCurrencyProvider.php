<?php

namespace Modules\Core\CurrencyRateUpdaters;

class OpenExchangeRatesPaidCurrencyProvider extends OpenExchangeRatesFreeCurrencyProvider
{

    /**
     * @inheritDoc
     */

    public function getRates(array $codes, string $base_code = null) : array
    {
        $base_codes = $codes;
        if ($base_code) {
            $base_codes = [$base_code];
        }
        $base_codes[] = 'USD';
        $result = [];
        foreach ($base_codes as $base_code) {
            $rates = $this->fetchRates($base_code);
            $result += $this->prepareResult($rates, $codes, $base_code);
        }
        return $result;
    }

    /**
     * Getting name of Currency Provider
     * @return string
     */
    public static function getName()
    {
        return 'OpenExchangeRates Paid';
    }

    /**
     * Getting alias of Currency Provider
     * @return string
     */
    public static function getAlias()
    {
        return 'openexchangerates_paid';
    }
}
