<?php

namespace Modules\Core\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Observers\DepositObserver;
use Modules\Core\Observers\OperationObserver;
use Modules\Core\Observers\TicketMessageObserver;
use Modules\Core\Observers\UserObserver;
use Modules\Core\Observers\WalletObserver;
use Modules\Core\ZEngine;

class CoreServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() : void
    {
        $this->registerConfig();
        $this->app->singleton('zengine', static function () {
            return new ZEngine();
        });
        $this->app->registerDeferredProvider(BindingsServiceProvider::class);
        $this->registerProviders();
    }

    /**
     * @throws BindingResolutionException
     */
    public function schedule()
    {
        $schedule = $this->app->make(Schedule::class);
        $schedule->command('currencies:rates-update')->dailyAt('12:00');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    public function boot()
    {
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');

        Relation::morphMap([
            'deposit'   => app('zengine')->modelClass('Deposit'),
            'user'      => app('zengine')->modelClass('User'),
            'plan'      => app('zengine')->modelClass('Plan'),
            'operation' => app('zengine')->modelClass('Operation'),
        ]);
        app('zengine')->model('Deposit')->observe(DepositObserver::class);
        app('zengine')->model('Operation')->observe(OperationObserver::class);
        app('zengine')->model('User')->observe(UserObserver::class);
        app('zengine')->model('Wallet')->observe(WalletObserver::class);
        app('zengine')->model('TicketMessage')->observe(TicketMessageObserver::class);

        $this->app->booted(function () {
            $this->schedule();
        });
    }

    /**
     * Register core service providers
     */
    public function registerProviders() : void
    {
        $this->app->register(AuthServiceProvider::class);
        $this->app->register(EventServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('zengine.php'),
        ], 'config');
        $this->mergeConfigFrom(__DIR__.'/../Config/config.php', 'zengine');
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/core');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path.'/modules/core';
        }, \Config::get('view.paths')), [$sourcePath]), 'core');
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__.'/../Database/factories');
        }
    }
}
