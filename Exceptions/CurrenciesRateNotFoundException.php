<?php


namespace Modules\Core\Exceptions;


class CurrenciesRateNotFoundException extends \Exception
{
    public function __construct($message = "Currencies rate not find")
    {
        parent::__construct($message, 500);
    }
}
