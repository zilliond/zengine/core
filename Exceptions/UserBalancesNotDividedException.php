<?php


namespace Modules\Core\Exceptions;

use Throwable;

class UserBalancesNotDividedException extends \Exception
{
    public function __construct($message = "User balances not divided", $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
