<?php


namespace Modules\Core\OperationTypes;


use Modules\Core\Models\Operation;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;
use Modules\Core\Services\Contracts\UserServiceInterface;

class ReferralPayOperationType extends OperationType
{
    public function success(Operation $operation) : Operation
    {
        $wallet = null;
        $user = $operation->user;
        if ('divided' === setting('balances_type')) {
            $wallet = $user->wallets()->firstOrCreate([
                'payment_system_id' => $operation->wallet->payment_system_id,
            ]);
        }
        app(UserServiceInterface::class)->putMoney($operation, $user, $wallet);
        $operation = parent::success($operation);
        $operation->user->referrals_earned += app(CurrencyServiceInterface::class)->convertToDefault($operation->amount, $operation->currency);
        $operation->user->save();
        return $operation;
    }
}
