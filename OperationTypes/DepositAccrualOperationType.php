<?php


namespace Modules\Core\OperationTypes;


use Modules\Core\Models\Operation;
use Modules\Core\Services\Contracts\OperationServiceInterface;
use Modules\Core\Services\Contracts\UserServiceInterface;

class DepositAccrualOperationType extends OperationType
{
    public function success(Operation $operation) : Operation
    {
        app(UserServiceInterface::class)->putMoney($operation);
        app(OperationServiceInterface::class)->referralPay($operation);
        return parent::success($operation);
    }
}
