<?php


namespace Modules\Core\OperationTypes;


use Modules\Core\Models\Operation;

abstract class OperationType
{

    public function canCreate(Operation $operation) : bool
    {
        return true;
    }

    public function canCancel(Operation $operation) : bool
    {
        return true;
    }

    public function canReject(Operation $operation) : bool
    {
        return true;
    }

    public function canInProgress(Operation $operation) : bool
    {
        return true;
    }

    public function canSuccess(Operation $operation) : bool
    {
        return true;
    }


    public function create(Operation $operation) : Operation
    {
        $operation->status = app('zengine')->modelClass('Operation')::STATUS_CREATED;
        $operation->save();
        return $operation;
    }

    public function cancel(Operation $operation) : Operation
    {
        $operation->status = app('zengine')->modelClass('Operation')::STATUS_CANCELED;
        $operation->save();
        return $operation;
    }

    public function reject(Operation $operation) : Operation
    {
        $operation->status = app('zengine')->modelClass('Operation')::STATUS_REJECTED;
        $operation->save();
        return $operation;
    }

    public function inProgress(Operation $operation) : Operation
    {
        $operation->status = app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS;
        $operation->save();
        return $operation;
    }

    public function success(Operation $operation) : Operation
    {
        $operation->status = app('zengine')->modelClass('Operation')::STATUS_SUCCESS;
        $operation->save();
        return $operation;
    }

}
