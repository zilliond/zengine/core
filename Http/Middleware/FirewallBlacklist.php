<?php

namespace Modules\Core\Http\Middleware;

use Cache;
use Closure;
use Illuminate\Database\QueryException;
use Illuminate\Validation\UnauthorizedException;
use Log;
use Symfony\Component\HttpFoundation\IpUtils;

class FirewallBlacklist extends \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode
{
    /**
     * @var array except routes
     */
    protected $except = [];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $blacklist = Cache::remember('blacklist', 60, static function () {
                return app('zengine')->model('BlacklistEntry')->pluck('ip');
            });
        } catch (QueryException $e) {
            Log::info('DB CONNECTION ERROR');

            return $next($request);
        }
        if ($blacklist->count()) {
            if ($this->inExceptArray($request)) {
                return $next($request);
            }
            if (IpUtils::checkIp($request->ip(), $blacklist->toArray())) {
                throw new UnauthorizedException("Your ip {$request->ip()} banned");
            }
        }

        return $next($request);
    }
}
