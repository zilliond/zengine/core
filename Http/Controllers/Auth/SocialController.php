<?php

namespace Modules\Core\Http\Controllers\Auth;

use Cookie;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Models\SocialAccount;
use Modules\Core\Services\Contracts\SocialAuthServiceInterface;

class SocialController extends Controller
{
    /**
     * @param $provider
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect($provider) : \Symfony\Component\HttpFoundation\RedirectResponse
    {
        $socialService = app('social_auth');
        /** @var SocialAuthServiceInterface $socialService */
        return $socialService->driver($provider)->redirect();
    }

    public function callback($provider) : string
    {
        $socialService = app('social_auth');
        /** @var \Modules\Core\Services\SocialAuthServiceInterface $socialService */
        $userSocial = $socialService->driver($provider)->stateless()->user();
        \Debugbar::info($userSocial);

        $account = app('zengine')->model('SocialAccount')->where('provider', $provider)
            ->where('provider_user_id', $userSocial->getId())
            ->first();
        /** @var $account SocialAccount */
        if ($account) {
            \Auth::login($account->user);
            return redirect(route('home'));
        }

        $user = app('zengine')->model('User')->where('email', $userSocial->getEmail())->first();

        //Create user if dont'exist
        if (!$user) {
            $inviter = app('zengine')->model('User')->find(Cookie::get('inviter'));
            return view('auth.social_register', [
                'inviter'   => $inviter,
                'provider'  => $provider,
                'social'    => $userSocial
            ]);
        }

        //Create social account
        $user->social_accounts()->create([
            'provider_user_id' => $userSocial->getId(),
            'provider'         => $provider
        ]);

        \Auth::login($user);
        return redirect(route('home'));
    }

    public function logout($provider) : string
    {
        $socialService = app('social_auth');
        /** @var \Modules\Core\Services\SocialAuthServiceInterface $socialService */
        $userSocial = $socialService->driver($provider)->stateless()->user();
        \Debugbar::info($userSocial);

        /** @var $account SocialAccount */
        $account = app('zengine')->model('SocialAccount')->where('provider', $provider)
            ->where('provider_user_id', $userSocial->getId())
            ->first();

        if ($account) {
            \Auth::logout($account->user);
            $account->delete();
        }

        return redirect(route('home'));
    }
}
