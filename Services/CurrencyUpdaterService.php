<?php


namespace Modules\Core\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Log;
use Modules\Core\CurrencyRateUpdaters\BaseCurrencyProvider;
use Modules\Core\CurrencyRateUpdaters\OpenExchangeRatesFreeCurrencyProvider;
use Modules\Core\Models\Currency;
use Modules\Core\Services\Contracts\CurrencyUpdaterServiceInterface;

class CurrencyUpdaterService implements CurrencyUpdaterServiceInterface
{
    /**
     * Available providers list
     * @var array
     */
    protected $providers = [];
    /**
     * @var \Modules\Core\CurrencyRateUpdaters\BaseCurrencyProvider
     */
    protected $provider;
    /**
     * @var \Modules\Core\Models\Currency|null
     */
    protected $defaultCurrency;

    public function __construct()
    {
        $this->providers = array_merge($this->providers, config('zengine.currency_updater.providers'));
        $this->setProvider(config('zengine.currency_updater.default_provider'));
    }

    public function addProvider($alias, $class)
    {
        $reflection = new \ReflectionClass($class);
        if (!$reflection->isSubclassOf(BaseCurrencyProvider::class)) {
            throw new \ReflectionException('Provider is not subclass of BaseCurrencyProvider');
        }
        $this->providers[$alias] = $reflection->getName();
        return $this;
    }

    public function generateMissingCrossPairs(array $pairs, string $cross_currency = 'USD')
    {
        $result = [];
        $currencies = app('zengine')->model('Currency')->all();
        $currenciesRates = app('zengine')->model('CurrencyRate')->all();
        foreach ($currencies as $base_currency) {
            foreach ($currencies as $target_currency) {
                $pair = $base_currency->code_iso . '_' . $target_currency->code_iso;
                if ($base_currency->id === $target_currency->id || isset($pairs[$pair])) {
                    continue;
                }
                $currencyRate = $currenciesRates
                    ->where('currency_id', $base_currency->code_iso)
                    ->where('target_currency_id', $target_currency->code_iso)->first();

                if ($currencyRate && $currencyRate->type == app('zengine')->modelClass('CurrencyRate')::TYPE_FIXED) {
                    continue;
                }
                $first_pair = Str::upper($base_currency->code_iso) . '_' . Str::upper($cross_currency);
                $second_pair = Str::upper($cross_currency) . '_' . Str::upper($target_currency->code_iso);
                if (isset($pairs[$first_pair]) && isset($pairs[$second_pair])) {
                    $result[$pair] = $pairs[$first_pair] * $pairs[$second_pair];
                } else {
                    \Log::info("Pairs with cross currency not exist {$first_pair} {$second_pair}");
                }
            }
        }
        return $result;
    }

    public function generateMissingReversePairs(array $pairs, array $codes)
    {
        $result = [];
        foreach ($codes as $from_code) {
            foreach ($codes as $to_code) {
                $pair = Str::upper($from_code) . '_' . Str::upper($to_code);
                $reverse_pair = Str::upper($to_code) . '_' . Str::upper($from_code);
                if ($from_code === $to_code) {
                    $result[$pair] = 1.0;
                    continue;
                }
                if (isset($pairs[$pair]) && !isset($pairs[$reverse_pair])) {
                    $result[$reverse_pair] = 1 / $pairs[$pair];
                }
            }
        }
        return $result;
    }

    public function getDefaultCurrency()
    {
        if (!$this->defaultCurrency) {
            $this->defaultCurrency = app('zengine')->model('Currency')->find(setting('currencies.default')); // TODO: GET default currency from config
        }
        return $this->defaultCurrency;
    }

    public function getDefaultDriver()
    {
        return OpenExchangeRatesFreeCurrencyProvider::class;
    }

    public function getProvider($alias = null)
    {
        if ($alias) {
            if (isset($this->providers[$alias])) {
                try {
                    return (new \ReflectionClass($this->providers[$alias]))->newInstance();
                } catch (\ReflectionException $e) {
                    throw new \DomainException('Provider can\'t created');
                }
            } else {
                throw new \DomainException('Provider with this alias is not exist');
            }
        }
        return $this->provider;
    }

    public function setProvider(string $alias) : CurrencyUpdaterServiceInterface
    {
        $this->provider = $this->getProvider($alias);
        return $this;
    }

    public function getProviders()
    {
        return $this->providers;
    }

    public function getProvidersWithNames()
    {
        $providers = $this->getProviders();
        $providers_labels = [];
        foreach ($providers as $alias => $provider) {
            /* @var \Modules\Core\CurrencyRateUpdaters\BaseCurrencyProvider $provider */
            $providers_labels[$alias] = $provider::getName();
        }
        return $providers_labels;
    }

    public function hasProvider(string $alias) : bool
    {
        return isset($this->providers[$alias]) && $this->getProvider($alias) instanceof BaseCurrencyProvider;
    }

    public function setCurrencyExchangePairs($pairs)
    {
        $currencies = app('zengine')->model('Currency')->pluck('id', 'code_iso');
        foreach ($pairs as $pair => $rate) {
            $codes = explode('_', $pair);
            $base_id = $currencies[$codes[0]];
            $target_id = $currencies[$codes[1]];
            $currencyRate = app('zengine')->model('CurrencyRate')->firstOrNew([
                'currency_id'        => $base_id,
                'target_currency_id' => $target_id,
            ]);
            Log::info("CurrencyUpdaterService: {$pair} type {$currencyRate->type}");
            if ($currencyRate->type === app('zengine')->modelClass('CurrencyRate')::TYPE_FIXED) {
                Log::info("CurrencyUpdaterService: {$pair} fixed, skipped");
            } else {
                $rate = round($rate, setting('currencies.rate.decimal_precision'));
                if ($currencyRate->rate !== $rate) {
                    $currencyRate->rate = $rate;
                    $currencyRate->save();
                    Log::info("CurrencyUpdaterService: {$pair} updated, {$rate}");
                } else {
                    Log::info("CurrencyUpdaterService: {$pair} not changed");
                }
            }
        }
    }

    public function updateAllRates() : Collection
    {
        $currencies = app('zengine')->model('Currency')->whereNotNull('update_provider')->where('auto_update_rate', 1)->get();
        \Log::info("CurrencyUpdaterService: {$currencies->count()} currencies rates updating");
        $providers = $currencies->pluck('update_provider');

        $pairs = collect();
        foreach ($providers as $provider_alias) {
            if (!$this->hasProvider($provider_alias)) {
                Log::info("CurrencyUpdaterService: {$provider_alias} not exist/valid");
                continue;
            }
            $provider = $this->getProvider($provider_alias);
            $provider_currencies = $currencies->where('update_provider', $provider_alias)->pluck('code_iso')->toArray();
            $provider_currencies[] = $this->getDefaultCurrency()->code_iso;
            /** TODO: Maybe set base currency */
            $rates = $provider->getRates($provider_currencies);
            \Log::info($provider_alias);
            \Log::info($provider_currencies);
            \Log::info($rates);
            $rates_with_reverse = $rates + $this->generateMissingReversePairs($rates, $provider_currencies);
            \Log::info($rates_with_reverse);
            $pairs = $pairs->merge($rates_with_reverse);
        }
        $pairs = $pairs->merge($this->generateMissingCrossPairs($pairs->toArray()));
        \Log::info("CurrencyUpdaterService: {$pairs->count()} currency exchange pairs updating");

        $this->setCurrencyExchangePairs($pairs);
        return $currencies->load('rates');
    }

    public function updateRate(Currency $currency) : Currency
    {
        $provider = $this->getProvider($currency->update_provider);
        $pairs = collect($provider->getRates([$currency->code_iso]));
        \Log::info("CurrencyUpdaterService: {$pairs->count()} currency exchange pairs updating");
        $this->setCurrencyExchangePairs($pairs);
        return $currency->load('rates');
    }

    public function updateRateByCode($code) : Currency
    {
        $currency = app('zengine')->model('Currency')->where('code_iso', $code)->where('code', $code)->firstOrFail();
        /* @var Currency $currency */
        return $this->updateRate($currency);
    }
}
