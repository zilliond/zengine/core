<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Exceptions;
use Modules\Core\Exceptions\BalanceException;
use Modules\Core\Exceptions\BalanceTypeNotFindException;
use Modules\Core\Exceptions\CanPayWalletException;
use Modules\Core\Exceptions\OperationWalletInvalidException;
use Modules\Core\Models\Currency;
use Modules\Core\Models\Operation;
use Modules\Core\Models\User;
use Modules\Core\Models\Wallet;

interface UserServiceInterface
{
    public function onCreate(User $user);

    public function createReferral(User $user, User $inviter, int $level = 1);

    /**
     * @return int
     */
    public function onlineUsers();

    /**
     * @param  Operation  $operation
     * @param  User|null  $user
     * @param  Wallet|null  $wallet
     * @throws BalanceTypeNotFindException
     * @throws OperationWalletInvalidException
     */
    public function putMoney(Operation $operation, User $user = null, Wallet $wallet = null) : void;

    /**
     * @param  Operation  $operation
     * @throws BalanceException
     * @throws BalanceTypeNotFindException
     * @throws Exceptions\UserBalancesNotDividedException
     * @throws OperationWalletInvalidException
     */
    public function takeMoney(Operation $operation) : void;

    /**
     * @param  User  $user
     * @param  float  $amount
     * @param  Currency|null  $currency
     * @param  Wallet|null  $wallet
     * @return bool
     * @throws BalanceException
     * @throws BalanceTypeNotFindException
     * @throws CanPayWalletException
     */
    public function canPayFrom(User $user, float $amount, Currency $currency = null, Wallet $wallet = null) : bool;

    /**
     * @param  \Modules\Core\Models\User  $user
     * @param  float  $amount
     * @param  Currency  $currency
     * @return bool
     * @throws BalanceException
     */
    public function canPayFromBalance(User $user, float $amount, Currency $currency = null) : bool;

    /**
     * @param  \Modules\Core\Models\User  $user
     * @param  float  $amount
     * @param  Currency|null  $currency
     * @param  Wallet|null  $wallet
     * @return bool
     * @throws BalanceException
     * @throws CanPayWalletException
     */
    public function canPayFromWallet(
        User $user,
        float $amount,
        Currency $currency = null,
        Wallet $wallet = null
    ) : bool;

    public function getReferralEarned(User $user, User $referral);

    public function getFlattenReferrals(User $user);

    public function locked(User $user);

    public function unlock(User $user, $code);
}
