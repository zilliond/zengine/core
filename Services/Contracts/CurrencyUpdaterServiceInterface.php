<?php

namespace Modules\Core\Services\Contracts;

use Illuminate\Support\Collection;
use Modules\Core\CurrencyRateUpdaters\BaseCurrencyProvider;
use Modules\Core\Models\Currency;
use Modules\Core\Services\CurrencyManager;

interface CurrencyUpdaterServiceInterface
{
    /**
     * Add provider
     * @param  string  $alias
     * @param  string|Object  $class
     * @return CurrencyUpdaterServiceInterface
     * @throws \ReflectionException
     */
    public function addProvider($alias, $class);

    /**
     * helper for generate missing cross currency rate pairs
     * @param  array  $pairs  Pairs array in format ['USD_EUR' => 0.9, 'EUR_USD' => 1.1]
     * @param  string  $cross_currency  (optional) Intermediate currency, default USD
     * @return array ex: ['USD_EUR' => 0.9, 'EUR_USD' => 1.1]
     */
    public function generateMissingCrossPairs(array $pairs, string $cross_currency = 'USD');

    /**
     * helper for generate missing cross currency rate pairs
     * @param  array  $pairs  Pairs array in format ['USD_EUR' => 0.9, 'EUR_USD' => 1.1]
     * @param  array  $codes  Codes list ['USD', 'eur', 'rub']
     * @return array ex: ['USD_EUR' => 0.9, 'EUR_USD' => 1.1]
     */
    public function generateMissingReversePairs(array $pairs, array $codes);

    /**
     * @return \Modules\Core\Models\Currency
     */
    public function getDefaultCurrency();

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver();

    /**
     * @param  string|null  $alias
     * @return BaseCurrencyProvider|object
     */
    public function getProvider($alias = null);

    /**
     * @param  string  $alias
     * @return CurrencyUpdaterServiceInterface
     * @throws \DomainException
     */
    public function setProvider(string $alias) : CurrencyUpdaterServiceInterface;

    public function getProviders();

    /**
     * Get providers list in alias => name
     */
    public function getProvidersWithNames();

    /**
     * Check if exist and valid provider by alias
     * @param  string  $alias
     * @return bool
     */
    public function hasProvider(string $alias) : bool;

    /**
     * @param  Collection|array  $pairs
     */
    public function setCurrencyExchangePairs($pairs);

    /**
     * Update all rates
     *
     * @return Collection
     */
    public function updateAllRates() : Collection;

    /**
     * Update currency rate to current
     *
     * @param  Currency  $currency
     * @return Currency
     */
    public function updateRate(Currency $currency) : Currency;

    /**
     * Update currency rate by code to current
     *
     * @param $code
     * @return Currency
     */
    public function updateRateByCode($code) : Currency;
}
