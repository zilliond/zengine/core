<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Core\Models\Referral
 *
 * @property int $id
 * @property int $user_id
 * @property int $referral_id
 * @property int $level
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Models\User $inviter
 * @property-read \Modules\Core\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral whereReferralId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Referral whereUserId($value)
 * @mixin \Eloquent
 * @property int $current_count
 * @method static \Illuminate\Database\Eloquent\Builder|Referral whereCurrentCount($value)
 */
class Referral extends Model
{
    protected $fillable = [
        'user_id',
        'referral_id',
        'level',
        'current_count',
        'created_at',
    ];

    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }

    public function inviter()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }
}
