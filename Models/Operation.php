<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Core\Models\Traits\Convert;

/**
 * Modules\Core\Models\Operation
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property int $currency_id
 * @property int|null $payment_system_id
 * @property array|null $payment_system_data
 * @property string $type
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $status_changed_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $target_type
 * @property int|null $target_id
 * @property-read \Modules\Core\Models\Currency $currency
 * @property-read mixed $default_amount
 * @property-read \Modules\Core\Models\PaymentSystem|null $payment_system
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $target
 * @property-read \Modules\Core\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation currencyAmountFilter($min_amount = 0, $max_amount = 0, $base_currency = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation requestFilter()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation wherePaymentSystemData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation wherePaymentSystemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereStatusChangedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereTargetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereTargetType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $wallet_id Use only if balances divided
 * @property-read \Modules\Core\Models\Wallet|null $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereWalletId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation status($status)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation type($type)
 * @property string|null $comment
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Operation whereComment($value)
 */
class Operation extends Model
{
    use Convert;

    public const TYPE_USER_REFILL = 'user_refill';
    public const TYPE_USER_WITHDRAW = 'user_withdraw';
    public const TYPE_USER_TRANSFER = 'user_transfer';
    public const TYPE_USER_BONUS = 'user_bonus';
    public const TYPE_DEPOSIT_OPEN = 'deposit_open';
    public const TYPE_DEPOSIT_ACCRUAL = 'deposit_accrual';
    public const TYPE_DEPOSIT_CLOSE = 'deposit_close';
    public const TYPE_REFERRAL_PAY = 'referral_pay';

    public const STATUS_CREATED = 'created';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_IN_PROGRESS = 'in_progress';
    public const STATUS_SUCCESS = 'success';

    protected $casts = [
        'status_changed_at'     => 'datetime',
        'payment_system_data'   => 'array'
    ];

    protected $fillable = [
        'user_id',
        'amount',
        'currency_id',
        'wallet_id',
        'type',
        'status',
        'payment_system_id',
        'payment_system_data',
        'comment',
        'status_changed_at',
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'default_amount'
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }

    /**
     * @return BelongsTo
     */
    public function wallet() : BelongsTo
    {
        return $this->belongsTo(app('zengine')->modelClass('Wallet'));
    }

    /**
     * @return BelongsTo
     */
    public function payment_system()
    {
        return $this->belongsTo(app('zengine')->modelClass('PaymentSystem'));
    }

    /**
     * @return BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(app('zengine')->modelClass('Currency'));
    }

    public function target()
    {
        return $this->morphTo('target');
    }

    public function getDefaultAmountAttribute()
    {
        if (!self::$currencyService) {
            self::$currencyService = app('currencies');
        }
        if (self::$currencyService->getDefaultCurrency()->id === $this->currency->id) {
            return $this->amount;
        } else {
            return self::$currencyService->convertToDefault($this->amount, $this->currency);
        }
    }

    /**
     * @param  Currency  $to
     * @return float|int
     */
    public function getAmountInCurrency(Currency $to)
    {
        return $this->convert($this->amount, $this->currency, $to);
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeRequestFilter($query)
    {
        return $query
            ->when(request('user_id'), function (Builder $query) {
                return $query->where('user_id', request('user_id'));
            })
            ->when(request('currency_id'), function (Builder $query) {
                return $query->where('currency_id', request('currency_id'));
            })
            ->when(request('payment_system_id'), function (Builder $query) {
                return $query->where('payment_system_id', request('payment_system_id'));
            })
            ->when(request('wallet_id'), function (Builder $query) {
                return $query->where('wallet_id', request('wallet_id'));
            })
            ->when(request('status'), function (Builder $query) {
                return $query->where('status', request('status'));
            })
            ->when(request('type'), function (Builder $query) {
                return $query->where('type', request('type'));
            });
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param  Builder  $query
     * @param  string  $type
     * @return Builder
     */
    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param  Builder  $query
     * @param  string  $status
     * @return Builder
     */
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param  Builder  $query
     * @param  int  $min_amount
     * @param  int  $max_amount
     * @param  Currency|null  $base_currency
     * @return Builder
     */
    public function scopeCurrencyAmountFilter($query, $min_amount = 0, $max_amount = 0, $base_currency = null)
    {
        $currenciesService = app('currencies');
        if (is_null($base_currency)) {
            $base_currency = $currenciesService->getDefaultCurrency();
        }
        return $query->when($min_amount || $max_amount, function (Builder $builder) use ($currenciesService, $base_currency, $min_amount, $max_amount) {
            return $builder->where(function ($builder) use ($currenciesService, $base_currency, $min_amount, $max_amount) {
                foreach ($currenciesService->getCurrencies() as $currency) {
                    $builder = $builder->orWhere(function (Builder $where) use ($currency, $currenciesService, $base_currency, $min_amount, $max_amount) {
                        if ((int) $currency->id === $currenciesService->getDefaultCurrency()) {
                            return $where
                                ->where('currency_id', $currency->id)
                                ->when($min_amount, function (Builder $when) use ($currency, $min_amount) {
                                    return $when->where('amount', '>=', $min_amount);
                                })
                                ->when($max_amount, function (Builder $when) use ($currency, $max_amount) {
                                    return $when->where('amount', '<=', $max_amount);
                                });
                        } else {
                            return $where
                                ->where('currency_id', $currency->id)
                                ->when($min_amount, function (Builder $when) use ($currenciesService, $base_currency, $currency, $min_amount) {
                                    return $when->where('amount', '>=', $currenciesService->convert($min_amount, $base_currency, $currency));
                                })
                                ->when($max_amount, function (Builder $when) use ($currenciesService, $base_currency, $currency, $max_amount) {
                                    return $when->where('amount', '<=', $currenciesService->convert($max_amount, $base_currency, $currency));
                                });
                        }
                    });
                }
                return $builder;
            });
        });
    }
}
