<?php

namespace Modules\Core\Models\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Core\Models\User;
use Modules\Core\Models\Wallet;

class WalletPolicy
{
    use HandlesAuthorization;

    public function can_modify(User $user, Wallet $wallet)
    {
        return
            ($wallet->user_id !== $user->id && $user->is_admin) ||
            ($wallet->wallet && $user->unlocked()) ||
            (!$wallet->wallet);
    }
}
