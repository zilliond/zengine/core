<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * Modules\Core\Models\FaqItem
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem visible()
 * @mixin \Eloquent
 * @property-read array $translations
 * @property int $id
 * @property array $title
 * @property array $text
 * @property int $hidden
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem whereHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FaqItem whereUpdatedAt($value)
 * @property array $question
 * @property array $answer
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\FaqItem whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\FaqItem whereQuestion($value)
 */
class FaqItem extends Model
{
    use HasTranslations;

    protected $fillable = [
        'question',
        'answer',
        'hidden',
    ];

    public $translatable = [
        'question',
        'answer',
    ];

    public function scopeVisible(Builder $builder)
    {
        return $builder->where('hidden', false);
    }
}
