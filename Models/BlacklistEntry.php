<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BlacklistEntry
 *
 * @property int $id
 * @property string $ip
 * @property string|null $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlacklistEntry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlacklistEntry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlacklistEntry query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlacklistEntry whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlacklistEntry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlacklistEntry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlacklistEntry whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlacklistEntry whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BlacklistEntry extends Model
{
    protected $fillable = [
        'ip',
        'comment'
    ];
}
