<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Core\Models\TicketMessage
 *
 * @property int $id
 * @property int $ticket_id
 * @property int $user_id
 * @property string $message
 * @property int $read
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Models\Ticket $ticket
 * @property-read \Modules\Core\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage whereTicketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\TicketMessage whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|TicketMessage whereAttachment($value)
 */
class TicketMessage extends Model
{
    protected $fillable = [
        'user_id', 'ticket_id', 'message', 'attachment', 'read'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(app('zengine')->modelClass('Ticket'));
    }
}
