<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\Traits\Encryptable;
use Modules\Core\Payments\PaymentSystemProvider;

/**
 * Modules\Core\Models\PaymentSystem
 *
 * @property int $id
 * @property string $name
 * @property string $provider
 * @property string|null $wallet
 * @property string|null $code
 * @property int $currency_id
 * @property float $balance
 * @property int $can_refill
 * @property float $refill_commission
 * @property float $min_refill_amount
 * @property float $max_refill_amount
 * @property int $can_withdraw
 * @property string $withdraw_type
 * @property float $min_withdraw_amount
 * @property float $max_withdraw_amount
 * @property float $withdraw_commission
 * @property int $enabled
 * @property int $need_wallet
 * @property array|null $credentials
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Models\Currency $currency
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\Wallet[] $wallets
 * @property-read int|null $wallets_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem enabled($enabled = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem refill($can_refill = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereCanRefill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereCanWithdraw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereCredentials($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereMaxRefillAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereMaxWithdrawAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereMinRefillAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereMinWithdrawAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereNeedWallet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereRefillCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereWallet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereWithdrawCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem whereWithdrawType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\PaymentSystem withdraw($can_withdraw = 1)
 * @mixin \Eloquent
 */
class PaymentSystem extends Model
{
    use Encryptable;

    protected $fillable = [
        'name',
        'balance',
        'provider',
        'wallet',
        'code',

        'can_refill',
        'min_refill_amount',
        'max_refill_amount',
        'refill_commission',

        'can_withdraw',
        'min_withdraw_amount',
        'max_withdraw_amount',
        'withdraw_commission',
        'withdraw_type',

        'currency_id',
        'enabled',
        'need_wallet',
        'credentials',
    ];

    protected $casts = [
        'credentials' => 'array',
    ];

    protected $encryptable = [
        'credentials'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(app('zengine')->modelClass('Currency'));
    }

    public function wallets()
    {
        return $this->hasMany(app('zengine')->modelClass('Wallet'), 'payment_system_id');
    }

    /**
     * @return \Modules\Core\Payments\PaymentSystemProvider|null
     */
    public function getProvider() : ?PaymentSystemProvider
    {
        if (!$this->provider) {
            return null;
        }
        $operationService = app('operations');
        $provider = $operationService->getProvider($this->provider);
        if ($this->credentials && is_array($this->credentials)) {
            $provider->setCredentials($this->credentials);
        }
        return $provider;
    }

    /**
     * Scope a query to only include enabled payment systems.
     *
     * @param  self  $query
     * @param  int  $enabled
     * @return self
     */
    public function scopeEnabled($query, $enabled = 1)
    {
        return $query->where('enabled', $enabled)->where('can_refill', 1);
    }

    /**
     * Scope a query to only include can refill payment systems.
     *
     * @param  self  $query
     * @param  int  $can_refill
     * @return self
     */
    public function scopeRefill($query, $can_refill = 1)
    {
        return $query->enabled()->where('can_refill', $can_refill);
    }

    /**
     * Scope a query to only include can withdraw payment systems.
     *
     * @param  self  $query
     * @param  int  $can_withdraw
     * @return self
     */
    public function scopeWithdraw($query, $can_withdraw = 1)
    {
        return $query->enabled()->where('can_withdraw', $can_withdraw);
    }
}
