<?php

namespace Modules\Core\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;
use Modules\Core\Models\Traits\Convert;

/**
 * Modules\Core\Models\Deposit
 *
 * @property int $id
 * @property float $amount
 * @property float $profit
 * @property int $currency_id
 * @property int $plan_id
 * @property int $user_id
 * @property int $passed_accruals
 * @property int $left_accruals
 * @property \Illuminate\Support\Carbon|null $last_accrual_at
 * @property \Illuminate\Support\Carbon|null $next_accrual_at
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Modules\Core\Models\Currency $currency
 * @property-read \Modules\Core\Models\Plan $plan
 * @property-read \Modules\Core\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Deposit onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereLastAccrualAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereLeftAccruals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereNextAccrualAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit wherePassedAccruals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereProfit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Deposit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Deposit withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Modules\Core\Models\Wallet $wallet
 * @property int $wallet_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Deposit whereWalletId($value)
 */
class Deposit extends Model
{
    use SoftDeletes, Convert;

    public $table = 'deposits';

    protected $dates = ['deleted_at', 'last_accrual_at', 'next_accrual_at'];

    public const STATUS_OPEN = 'open';
    public const STATUS_CLOSED = 'closed';

    public $fillable = [
        'amount',
        'profit',
        'plan_id',
        'currency_id',
        'user_id',
        'wallet_id',
        'status',
        'passed_accruals',
        'left_accruals',
        'last_accrual_at',
        'next_accrual_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'amount'            => 'float',
        'plan_id'           => 'integer',
        'user_id'           => 'integer',
        'left_accruals'     => 'integer',
        'last_accrual_at'   => 'datetime',
        'next_accrual_at'   => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'amount'            => 'required|min:0.00001',
            'profit'            => 'required|min:0',
            'plan_id'           => 'required|integer|exists:plans,id',
            'currency_id'       => 'required|integer|exists:currencies,id',
            'user_id'           => 'required|integer|exists:users,id',
            'status'            => [
                Rule::in(self::statuses())
            ],
            'passed_accruals'   => 'required|integer|min:0',
            'left_accruals'     => 'required|integer|min:0',
            'last_accrual_at'   => 'nullable|date_format:Y-m-d H:i:s',
            'next_accrual_at'   => 'nullable|date_format:Y-m-d H:i:s',
        ];
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_OPEN,
            self::STATUS_CLOSED
        ];
    }

    /**
     * @return BelongsTo
     **/
    public function plan()
    {
        return $this->belongsTo(app('zengine')->modelClass('Plan'), 'plan_id', 'id')->withTrashed();
    }

    /**
     * @return BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(app('zengine')->modelClass('Currency'), 'currency_id', 'id');
    }

    /**
     * @return BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'), 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     **/
    public function wallet()
    {
        return $this->belongsTo(app('zengine')->modelClass('Wallet'));
    }

    /**
     * @param  Currency  $to
     * @return float|int
     */
    public function getAmountInCurrency(Currency $to)
    {
        return $this->convert($this->amount, $this->currency, $to);
    }

    /**
     * @param  Currency  $to
     * @return float|int
     */
    public function getProfitInCurrency(Currency $to)
    {
        return $this->convert($this->profit, $this->currency, $to);
    }
}
