<?php

namespace Modules\Core\Payments;

use authDTO;
use Exception;
use getBalances;
use Illuminate\Http\Request;
use Log;
use MerchantWebService;
use Modules\Core\Exceptions;
use Modules\Core\Exceptions\OperationException;
use Modules\Core\Exceptions\PaymentSystemException;
use Modules\Core\Models\Operation;
use sendMoney;
use sendMoneyRequest;
use validationSendMoney;

class AdvancedCashProvider extends PaymentSystemProvider
{
    /* @inheritDoc */
    public $can_auto_withdraw = true;

    /* @inheritDoc */
    public $can_semi_auto_withdraw = true;

    public const SCI_ACTION = 'https://wallet.advcash.com/sci/';

    /** @inheritDoc */
    public function getName() : string
    {
        return 'Payeer';
    }
    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws Exceptions\OperationException
     */
    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['sci_account', 'sci_name', 'sci_key']);
        $form_data = [
            'ac_account_email'      => $this->getCredential('sci_account'),
            'ac_sci_name'           => $this->getCredential('sci_name'),
            'ac_amount'             => number_format($operation->amount, 2, '.', ''),
            'ac_currency'           => $operation->currency->code_iso,
            'ac_order_id'           => $operation->id,
            'ac_comments'           => '',
            'ac_status_url'         => route('payment_systems.ipn'),
            'ac_success_url'        => route('cabinet.refill', ['status' => 'success']),
            'ac_fail_url'           => route('cabinet.refill', ['status' => 'error']),
            'ac_fail_url_method'    => 'GET',
        ];
        $signData = [
            $form_data['ac_account_email'],
            $form_data['ac_sci_name'],
            $form_data['ac_amount'],
            $form_data['ac_currency'],
            $this->getCredential('sci_key'),
            $form_data['ac_order_id'],
        ];
        $form_data['ac_sign'] = hash('sha256', implode(':', $signData));
        $operation->payment_system_data = [
            'action'    => self::SCI_ACTION,
            'method'    => 'POST',
            'type'      => 'form',
            'form_data' => $form_data
        ];
        $operation->save();

        return $operation;
    }

    /**
     * @param  Request  $request
     * @return bool
     */
    public function canHandle(Request $request) : bool
    {
        Log::channel('payment_systems')->info("AdvancedCashProviderAdvancedCashProvider|canHandle", ['request' => $request->all()]);
        return $request->has('ac_order_id') && $request->has('ac_hash');
    }

    /**
     * @param  Request  $request
     * @throws Exceptions\BalanceException
     * @throws Exceptions\BalanceTypeNotFindException
     * @throws Exceptions\OperationWalletInvalidException
     * @throws OperationException
     * @throws PaymentSystemException
     */
    public function handle(Request $request)
    {
        $this->checkCredentials(['sci_key']);
        if (!in_array($request->ip(), ['50.7.115.5', '51.255.40.139'])) {
            throw new PaymentSystemException('AdvancedCashProviderAdvancedCashProvider| request ip mismatch');
        };
        Log::channel('payment_systems')->info("AdvancedCashProviderAdvancedCashProvider|Handler|start", ['request' => $request->all()]);
        $operation = app('zengine')->model('Operation')->find($request->get('ac_order_id'));
        if (!$operation) {
            Log::channel('payment_systems')
                ->info("AdvancedCashProviderAdvancedCashProvider|Handler|Operation {$request->get('ac_order_id')} not find - request", ['request' => $request->all()]);
            exit;
        }
        $hashData = [
            $request->get('ac_transfer'),
            $request->get('ac_start_date'),
            $request->get('ac_sci_name'),
            $request->get('ac_src_wallet'),
            $request->get('ac_dest_wallet'),
            $request->get('ac_order_id'),
            $request->get('ac_amount'),
            $request->get('ac_merchant_currency'),
            $this->getCredential('sci_key')
        ];
        $hash = hash('sha256', implode(':', $hashData));
        ;
        Log::channel('payment_systems')->info("AdvancedCashProviderAdvancedCashProvider|handle|Operation {$operation->id} | Generated hash $hash");
        $operation->payment_system_data = $operation->payment_system_data + ['hash' => $hash];
        $operation->save();
        if ($request->get('ac_hash') === $hash) {
            Log::channel('payment_systems')->info("AdvancedCashProviderAdvancedCashProvider|Handler|Operation {$operation->id} success");
            $this->operationService->typeByOperation($operation)->success($operation);
            exit;
        }
        Log::channel('payment_systems')->info("AdvancedCashProviderAdvancedCashProvider|Handler|Operation {$operation->id} error");
        exit;
    }

    /**
     * @return float
     * @throws Exceptions\OperationException
     * @throws Exception
     */
    public function getBalance() : float
    {
        $this->checkCredentials(['api_name', 'sci_account', 'api_key']);
        require_once 'MerchantWebService.php';
        $merchantWebService = new MerchantWebService();
        $arg0 = new authDTO();
        $arg0->apiName = $this->getCredential('api_name');
        $arg0->accountEmail = $this->getCredential('sci_account');
        $arg0->authenticationToken = $merchantWebService->getAuthenticationToken($this->getCredential('api_key'));

        $getBalances = new getBalances();
        $getBalances->arg0 = $arg0;
        $getBalancesResponse = $merchantWebService->getBalances($getBalances);
        if ($getBalancesResponse && count($getBalancesResponse->return) && isset($getBalancesResponse->return[0])) {
            return (float) $getBalancesResponse->return[0]->amount;
        }
        return 0.00;
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws Exceptions\OperationException
     */
    public function makeWithdraw(Operation $operation) : Operation
    {
        $this->checkCredentials(['api_name', 'sci_account', 'api_key']);
        if (!isset($operation->payment_system_data['target'])) {
            throw new OperationException('AdvancedCashProvider|makeWithdraw|Not set target account');
        }
        require_once 'MerchantWebService.php';
        $merchantWebService = new MerchantWebService();
        $arg0 = new authDTO();
        $arg0->apiName = $this->getCredential('api_name');
        $arg0->accountEmail = $this->getCredential('sci_account');
        $arg0->authenticationToken = $merchantWebService->getAuthenticationToken($this->getCredential('api_key'));

        $arg1 = new sendMoneyRequest();
        $arg1->amount = $operation->amount;
        $arg1->currency = $operation->currency->code_iso;
        $arg1->email = $operation->payment_system_data['target'];
        $arg1->note = 'Withdraw from '.config('app.name');
        $arg1->savePaymentTemplate = false;
        $validationSendMoney = new validationSendMoney();
        $validationSendMoney->arg0 = $arg0;
        $validationSendMoney->arg1 = $arg1;
        $sendMoney = new sendMoney();
        $sendMoney->arg0 = $arg0;
        $sendMoney->arg1 = $arg1;
        try {
            $merchantWebService->validationSendMoney($validationSendMoney);
            $sendMoneyResponse = $merchantWebService->sendMoney($sendMoney);
            if ($result = (string) $sendMoneyResponse->return) {
                $operation->payment_system_data += ['transaction_id' => $result];
            } else {
                $operation->payment_system_data += ['message' => $result];
            }
        } catch (Exception $e) {
            $operation->payment_system_data += ['error' => $e->getMessage()];
        }
        $operation->save();
        return $operation;
    }

    public function getCredentialsKeys() : array
    {
        return [
            'sci_account',
            'sci_name',
            'sci_key',
            'api_name',
            'api_key',
        ];
    }

    public function getCredentialsFields() : array
    {
        return [
            'sci_account'   => 'ID магазина',
            'sci_name'      => 'Название магазина',
            'sci_key'       => 'Приватный ключ магазина',
            'api_name'      => 'Название API',
            'api_key'       => 'API ключ',
        ];
    }
}
