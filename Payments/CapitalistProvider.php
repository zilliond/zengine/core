<?php


namespace Modules\Core\Payments;

use Illuminate\Http\Request;
use Log;
use Modules\Core\Exceptions\PaymentSystemException;
use Modules\Core\Models\Operation;

/**
 * NOT WORKING && NOT TESTED
 * Class CapitalistProvider
 * @package Modules\Core\Payments
 */
class CapitalistProvider extends PaymentSystemProvider
{
    /* @inheritDoc */
    public $can_auto_withdraw = false;

    /* @inheritDoc */
    public $can_semi_auto_withdraw = false;

    /**
     * @inheritDoc
     */
    public function getName() : string
    {
        return 'Capitalist';
    }

    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['merchant_id', 'merchant_secret']);
        $merchant_id = $this->getCredential('merchant_id');
        $merchant_secret = $this->getCredential('merchant_secret');
        $currency = $operation->currency->code_iso;
        $amount = $operation->amount;
        if (in_array($currency, ['USD', 'RUB', 'EUR'])) {
            $amount = number_format($amount, 2, '.', '');
        } else {
            if (in_array($this->currencyService->getDefaultCurrency()->code_iso, ['USD', 'RUB', 'EUR'])) {
                $currency = $this->currencyService->getDefaultCurrency()->code_iso;
                $amount = number_format($this->currencyService->convertToDefault($amount, $operation->currency), 2, '.', '');
            } else {
                $currency = 'USD';
                $currency_obj = app('zengine')->model('Currency')->where('code_iso', $currency)->firstOrFail();
                $amount = number_format($this->currencyService->convert($amount, $operation->currency, $currency_obj), 2, '.', '');
            }
        }
        $form_data = [
            'amount'        => $amount,
            'currency'      => $currency,
            'description'   => 'Refill',
            'merchantid'    => $merchant_id,
            'number'        => $operation->id,
            'opt_email'     => $operation->user->email,
        ];
        $sign = hash_hmac('md5', implode(':', $form_data), $merchant_secret);
        $form_data['sign'] = $sign;
        $operation->payment_system_data = [
            'action'    => "https://capitalist.net/merchant/pay",
            'method'    => 'POST',
            'type'      => 'form',
            'form_data' => $form_data
        ];
        $operation->save();

        return $operation;
    }

    public function canHandle(Request $request) : bool
    {
        try {
            $this->checkCredentials(['merchant_id']);
        } catch (PaymentSystemException $exception) {
            Log::channel('payment_systems')->info("CapitalistProvider|canHandle credentials error", ['request' => $request->all()]);
            return false;
        }
        Log::channel('payment_systems')->info("CapitalistProvider|canHandle", ['request' => $request->all()]);
        return $request->get('from') === 'capitalist' && $this->getCredential('merchant_id') === $request->get('merchant_id');
    }

    public function handle(Request $request)
    {
        $this->checkCredentials(['merchant_id', 'merchant_secret']);
        $merchant_id = $this->getCredential('merchant_id');
        $merchant_secret = $this->getCredential('merchant_secret');
        Log::channel('payment_systems')->info("CapitalistProvider|Handler|start", ['request' => $request->all()]);
        $operation = app('zengine')->model('Operation')->find($request->get('order_number'));
        if (!$operation) {
            Log::channel('payment_systems')
                ->info("CapitalistProvider|Handler|Operation {$request->get('order_number')} not find - request", ['request' => $request->all()]);
            exit;
        }
    }

    public function getCredentialsKeys() : array
    {
        return [
            'merchant_id',
            'merchant_secret'
        ];
    }

    public function getCredentialsFields() : array
    {
        return [
            'merchant_id'       => 'Идентификатор магазина',
            'merchant_secret'   => 'Секретный ключ магазина'
        ];
    }
}
