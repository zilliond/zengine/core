<?php


namespace Modules\Core\Payments;

use Modules\Core\Exceptions;
use Modules\Core\Exceptions\PaymentSystemException;
use Modules\Core\Models\Operation;
use Illuminate\Http\Request;
use Log;

class CoinPaymentsProvider extends PaymentSystemProvider
{
    private const SCI_ACTION = 'https://www.coinpayments.net/index.php';

    /** @inheritDoc */
    public function getName() : string
    {
        return 'Coin Payments';
    }
    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws Exceptions\OperationException
     */
    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['sci_id']);
        $form_data = [
            'cmd'           => '_pay',
            'reset'         => 1,
            'want_shipping' => 0,
            'enable_company'=> 0,
            'merchant'      => $this->getCredential('sci_id'),
            'currency'      => $operation->currency->code_iso,
            'amountf'       => $operation->amount,
            'item_name'     => 'Refill to account '.$operation->user_id,
            'invoice'       => $operation->id,
            'ipn_url'       => route('payment_systems.ipn'),
            'success_url'   => route('cabinet.refill', ['status' => 'success']),
            'cancel_url'    => route('cabinet.refill', ['status' => 'error']),
        ];
        $operation->payment_system_data = [
            'action'    => self::SCI_ACTION,
            'method'    => 'POST',
            'type'      => 'form',
            'form_data' => $form_data
        ];
        $operation->save();

        return $operation;
    }

    /**
     * @param  Request  $request
     * @return bool
     * @throws Exceptions\OperationException
     */
    public function canHandle(Request $request) : bool
    {
        try {
            $this->checkCredentials(['sci_id']);
        } catch (PaymentSystemException $exception) {
            Log::channel('payment_systems')->info("CoinPaymentsProvider|canHandle credentials error", ['request' => $request->all()]);
            return false;
        }
        Log::channel('payment_systems')->info("CoinPaymentsProvider|canHandle", ['request' => $request->all()]);
        return $request->filled('HTTP_HMAC') && $request->filled('merchant') && $this->getCredential('sci_id') === $request->get('merchant');
    }

    public function handle(Request $request)
    {
        return $request->getContent();
    }

    public function getCredentialsKeys() : array
    {
        return [
            'sci_id', 'sci_secret'
        ];
    }

    public function getCredentialsFields() : array
    {
        return [
            'sci_id'     => 'ID',
            'sci_secret' => 'Secret'
        ];
    }
}
